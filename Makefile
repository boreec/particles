CC=g++
LDFLAGS=`wx-config --cxxflags --libs`
SRC_DIR=src
SOURCES := $(SRC_DIR)/Properties.cpp
SOURCES += $(SRC_DIR)/BasicDrawPane.cpp $(SRC_DIR)/MainFrame.cpp $(SRC_DIR)/sma_app.cpp
SOURCES += $(SRC_DIR)/Environment.cpp
TARGETS=SMA_APP

all:$(TARGETS)

SMA_APP: $(SOURCES)
	$(CC) $^  $(LDFLAGS) -o $@

cleanall:
	rm -f $(TARGETS)
