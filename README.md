# 2D Particles collisions simulation

Particles collisions are simulated in a 2D discrete environment using a multi-agents system approach.

Author : [Cyprien Borée](https://boreec.fr)

![](http://boreec.fr/wp-content/uploads/2020/11/Particles-2.gif)

## Compilation

This project is written in **C++** and is compiled through a **Makefile**, so a suitable compiler must be used (such as gcc/g++).
Furthermore, the GUI library **wxWidgets** (>= 3.0) is used to display the environment in a window.

```
make # Build SMA_APP executable
make cleanall # Delete SMA_APP executable
```

## Execution

Simulation is constructed on a bunch of parameters such as :
- **AGENT_QUANTITY** : The amount of agents in the environment.
- **ENV_SIZE** : The square root size of the environment. If **ENV_SIZE** is 10, then environment can have at most 100 agents.
- **SEED** : Used to generate randomness. If the seed is set to `0`, a random seed is used.
- **TIMER_INTERVAL** : Time between every simulation update in milliseconds.
- **MIN_WINDOW_SIZE** : The square root size of the GUI window.
- **SCHEDULING** : Either `RANDOM` or `SEQUENTIAL`. It defines how agents will be updated.

All of these parameters can be written in a .property file using the following format :
```txt
PARAMETER=VALUE
PARAMETER=VALUE
...
```

You can check the already provided files in **examples/**.

```
$ ./SMA_APP examples/big_experiment.property
```