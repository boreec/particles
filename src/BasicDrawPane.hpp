#include "wx/wx.h"
#include "wx/sizer.h"
#include "Environment.hpp"

#ifndef __BASIC_DRAW_PANE_HPP__
#define __BASIC_DRAW_PANE_HPP__
class BasicDrawPane : public wxPanel
{
private:
  wxFrame* parent;
  Environment* env;
public:
  BasicDrawPane(wxFrame* parent, Environment* env);
    
  void paintEvent(wxPaintEvent & evt);
  void paintNow();  
  void render(wxDC& dc);
  void drawBackground(wxDC &dc);
  void drawAgents(wxDC &dc);
  
  // some useful events
    /*
     void mouseMoved(wxMouseEvent& event);
     void mouseDown(wxMouseEvent& event);
     void mouseWheelMoved(wxMouseEvent& event);
     void mouseReleased(wxMouseEvent& event);
     void rightClick(wxMouseEvent& event);
     void mouseLeftWindow(wxMouseEvent& event);
     void keyPressed(wxKeyEvent& event);
     void keyReleased(wxKeyEvent& event);
    */
    
  DECLARE_EVENT_TABLE()
};
#endif
