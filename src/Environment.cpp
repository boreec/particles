#include "Environment.hpp"

Environment::Environment(unsigned int w, unsigned int h, bool t){

  this->width = w;
  this->height = h;
  this->grid = new Agent** [h];

  for(int i = 0; i < h; ++i){
    this->grid[i] = new Agent* [w];
  }

  for(int i = 0; i < h; ++i){
    for(int j = 0; j < w; ++j){
      this->grid[i][j] = nullptr;
    }
  }
  this->toric = t;
}


void Environment::placeAgents(unsigned int agents, unsigned int seed){

  // Make sure the number of agents does not exceed environment capacity
  assert(agents <= this->width * this->height);
  srand(seed);
  
  while(agents > 0){
    int x = rand() % this->width;
    int y = rand() % this->height;

    if(this->grid[y][x] == nullptr){
      this->grid[y][x] = new Agent(x, y, (rand() % 3) - 1, (rand() % 3) - 1);
      this->agents.push_back(this->grid[y][x]);
      agents--;
    }
  }
}

std::string Environment::toString() const{
  std::string res;
  for(int i = 0; i < this->height; i++){
    for(int j = 0; j < this->width; j++){
      if(this->grid[i][j] != nullptr){
	res += "A\t";
      }
      else{
	res += "N\t";
      }
    }
    res += "\n";
  }
  return res;
}

void Environment::shuffleAgents(){
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine e(seed);
  std::shuffle(agents.begin(), agents.end(), e);
}

Agent::Agent(unsigned int x, unsigned int y, signed int stepX, signed int stepY){
  this->x = x;
  this->y = y;
  this->stepX = stepX;
  this->stepY = stepY;
  this->colour = wxColour(rand() % 256, rand() % 256, rand() % 256);
}

void Agent::decide(Environment *env){

  // Where the agents intend to go according to
  // its coordinates and steps.
  int targetX = x + stepX;
  int targetY = y + stepY;

  // rectify coordinates if the environment is considered toric.
  // no collisions with walls.

  if(env->isToric()){
    if(targetX == -1){
      targetX = env->getWidth() - 1;
    }
    else if(targetX == env->getWidth()){
      targetX = 0;
    }
    if(targetY == -1){
      targetY = env->getHeight() - 1;
    }
    else if(targetY == env->getHeight()){
      targetY = 0;
    }
  }
  if(-1 < targetX && targetX < env->getWidth() && -1 < targetY && targetY < env->getHeight()){
    if(env->getSquare(targetX, targetY) == nullptr){
      move(env, targetX, targetY);
    }else{
      collide(env->getSquare(targetX, targetY));
    }
  }else{
    // deal with wall-collision
    if(targetX == -1){
      targetX = 1;
      stepX *= -1;
    }
    else if(targetX == env->getWidth()){
      targetX = env->getWidth() - 2;
      stepX *= -1;
    }
    if(targetY == -1){
      targetY = 1;
      stepY *= -1;
    }
    else if(targetY == env->getHeight()){
      targetY = env->getHeight() - 2;
      stepY *= -1;
    }
    if(env->getSquare(targetX, targetY) == nullptr){
      move(env, targetX, targetY);
    }else{
      collide(env->getSquare(targetX, targetY));
    }
  }
}

void Agent::move(Environment *env, unsigned int newX, unsigned int newY){
  env->setSquare(newX, newY, this);
  env->setSquare(x, y, nullptr);
  x = newX;
  y = newY;
}

 void Agent::collide(Agent* a){
   this->stepX *= -1;
   this->stepY *= -1;
 }
