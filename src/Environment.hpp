#include <algorithm>
#include <cassert>
#include <chrono>
#include <cstdlib>
#include <random>
#include <string>
#include <vector>
#include "wx/colour.h"

#ifndef __ENVIRONMENT_HPP__
#define __ENVIRONMENT_HPP__

//forward declaration to prevent mutual declaration issue.
class Agent;

class Environment{
private:
  unsigned int width;
  unsigned int height;
  Agent*** grid;
  std::vector<Agent*> agents;
  bool toric;
public:
  Environment(unsigned int w, unsigned int h, bool t);
  void placeAgents(unsigned int quantity, unsigned int seed);
  std::string toString() const;
  std::vector<Agent*> getAgents() const {return this->agents;};
  void shuffleAgents();
  unsigned int getWidth() const {return this->width;};
  unsigned int getHeight() const {return this->height;};
  Agent* getSquare(unsigned int x, unsigned int y){return this->grid[y][x];};
  void setSquare(unsigned int x, unsigned int y, Agent *a){this->grid[y][x] = a;};
  bool isToric() const {return this->toric;};
};

class Agent{
private:
  unsigned int x;
  unsigned int y;
  signed int stepX;
  signed int stepY;
  wxColour colour;
public:
  Agent(unsigned int, unsigned int, signed int, signed int);

  void decide(Environment *env);
  void move(Environment *env, unsigned int newX, unsigned int newY);
  void collide(Agent *a);
  unsigned int getX() const {return this->x;};
  unsigned int getY() const {return this->y;};
  wxColour getColour() const {return this->colour;};
};
#endif
