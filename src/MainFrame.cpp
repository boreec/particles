#include "MainFrame.hpp"
#include <iostream>

MainFrame::MainFrame(Properties p) : wxFrame(NULL, -1, _T("SMA- Cyprien Borée")){
  this->tick_limit = p.TICK_LIMIT;
  this->random_scheduling = p.RANDOM_SCHEDULING;
  
  this->env = new Environment(p.ENV_SIZE, p.ENV_SIZE, p.TORUS);
  this->env->placeAgents(p.AGENT_QUANTITY, p.SEED);
  //std::cout << this->env->toString() << std::endl;
  this->drawPane = new BasicDrawPane(this, this->env);
  this->SetMinSize(wxSize(p.MIN_WINDOW_SIZE, p.MIN_WINDOW_SIZE));
  this->SetClientSize(p.MIN_WINDOW_SIZE, p.MIN_WINDOW_SIZE);
  m_timer.Bind(wxEVT_TIMER, &MainFrame::OnTimer, this);
  m_timer.Start(p.TIMER_INTERVAL);
}

void MainFrame::OnTimer(wxTimerEvent &e){
  if(this->random_scheduling){
    this->env->shuffleAgents();
  }
  
  for(auto const& a : this->env->getAgents()){
    a->decide(this->env);
  }
  this->drawPane->Refresh();
  
  this->tick++;
  if(this->tick_limit > 0){
    std::cout << "tick_limit: " << this->tick_limit << " current tick is " << this->tick << std::endl;
    if(this->tick >= this->tick_limit){
      m_timer.Stop();
    }
  }
}

