#include <string>
#include "wx/frame.h"
#include "wx/timer.h"
#include "Properties.hpp"
#include "BasicDrawPane.hpp"
#include "Environment.hpp"

#ifndef __MAIN_FRAME_HPP__
#define __MAIN_FRAME_HPP__
class MainFrame : public wxFrame {
public:
  MainFrame(Properties p);
  void OnTimer(wxTimerEvent &e);
private:
  wxTimer m_timer;
  BasicDrawPane* drawPane;
  Environment* env;
  Properties p;
  unsigned int tick = 0;
  unsigned int tick_limit = 0;
  bool random_scheduling = false;
};
#endif
