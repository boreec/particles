#include "Properties.hpp"
#include <iostream>

void Properties::parse(std::string filename){
  std::ifstream file(filename);
  if(file.is_open()){
    std::string line;
    while(std::getline(file, line)){
      std::string key = "";
      std::string value = "";
      
      std::size_t _found = line.find('=');
      if(_found != std::string::npos){
	key = line.substr(0, _found);
	value = line.substr(_found + 1, std::string::npos);

	// remove new_line char at the end
	if(value[value.length() - 1] == '\n'){
	  value.pop_back();
	}
	registerProperties(key, value);
      }else{
	std::cout << "bad format for property file" << std::endl;
      }
    }
    file.close();
  }else{
    std::cout << "Error opening file " << filename << std::endl;
  }
}

void Properties::registerProperties(std::string key, std::string value){
  if(key == "AGENT_QUANTITY"){
    this->AGENT_QUANTITY = stoi(value);
  }
  else if(key == "ENV_SIZE"){
    this->ENV_SIZE = stoi(value);
  }
  else if(key == "SEED"){
    this->SEED = stoi(value);
  }
  else if(key == "TIMER_INTERVAL"){
    this->TIMER_INTERVAL = stoi(value);
  }
  else if(key == "MIN_WINDOW_SIZE"){
    this->MIN_WINDOW_SIZE = stoi(value);
  }
  else if(key == "TORUS"){
    this->TORUS = value == "TRUE";
  }
  else if(key == "TICK_LIMIT"){
    this->TICK_LIMIT = stoi(value);
  }
  else if(key == "SCHEDULING"){
    this->RANDOM_SCHEDULING = value == "RANDOM";
  }
  else{
    std::cout << "Unknown key : " << key << std::endl; 
  }
}
