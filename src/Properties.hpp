#include <fstream>
#include <string>
#include <iostream>

#ifndef __PROPERTIES_HPP__
#define __PROPERTIES_HPP__
class Properties{
private:
  void registerProperties(std::string key, std::string value);
public:
  Properties() {};
  void parse(std::string filename);
  unsigned int AGENT_QUANTITY = 10;
  unsigned int ENV_SIZE = 10;
  unsigned int SEED = 0;
  unsigned int TIMER_INTERVAL = 200;
  unsigned int MIN_WINDOW_SIZE = 900;
  bool TORUS = false;
  bool RANDOM_SCHEDULING = false;
  unsigned int TICK_LIMIT = 0;
};
#endif
