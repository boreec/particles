#include "sma_app.hpp"

IMPLEMENT_APP(SMAApp)

bool SMAApp::OnInit(){
  Properties p;
  if(wxApp::argc > 1){
    p.parse(std::string(wxApp::argv[1]));
  }
  top_frame = new MainFrame(p);
  top_frame->Show(true);
  SetTopWindow(top_frame);
  return true;
}


