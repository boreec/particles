#include <wx/wx.h>

#include "MainFrame.hpp"
#include "Properties.hpp"

#ifndef __SMA_APP__
#define __SMA_APP__
class SMAApp : public wxApp {
private:
  MainFrame* top_frame;
public:
  virtual bool OnInit();
};
DECLARE_APP(SMAApp)
#endif
